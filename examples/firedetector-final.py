"""
    Fire detector for CanSAT drone vision subsystem.
"""

import cv2
import numpy as np

# Frame
FRAME_WIDTH = 640
FRAME_HEIGHT = 480

def main():
    print(f'OpenCV version: {cv2.__version__}')

    camera0 = cv2.VideoCapture(0, cv2.CAP_V4L2)
    camera0.set(cv2.CAP_PROP_FRAME_WIDTH, FRAME_WIDTH)
    camera0.set(cv2.CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT)

    bypass_gaussian = False

    while True:
        # 1. Capture frame from camera
        frame_ok, image_from_camera = camera0.read()
        if not frame_ok:
            continue

        # 2. Convert to grayscale
        image_grayscale = cv2.cvtColor(image_from_camera, cv2.COLOR_RGB2GRAY)

        # 3. Apply Gaussian Blur
        gaussian_blur_kernel = (11, 11)  # try (3,3) or (5,5) or (7,7) or (11,11)
        image_gaussian = cv2.GaussianBlur(image_grayscale, gaussian_blur_kernel, 0)

        # 4. Apply thresholding
        # 4.1 Bypass gaussian stage to show the difference
        if bypass_gaussian:
            res, image_threshold = cv2.threshold(image_grayscale, 180, 255, cv2.THRESH_BINARY)
        else:
            res, image_threshold = cv2.threshold(image_gaussian, 180, 255, cv2.THRESH_BINARY)

        # 5. Erode
        erode_kernel = np.ones((5, 5), "uint8")
        image_erode = cv2.erode(image_threshold, erode_kernel, iterations=1)

        # 6. Dilate some
        dilation_kernel = np.ones((5, 5), "uint8")
        image_dilation = cv2.dilate(image_erode, dilation_kernel, iterations=1)

        # 7. Find contours
        contours, hierarchy = cv2.findContours(image_dilation, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        # 8. Find largest contour
        largest_contour = None
        max_area = 0
        for idx, contour in enumerate(contours):
            area = cv2.contourArea(contour)
            if area > max_area:
                max_area = area
                largest_contour = contour

        if largest_contour is not None:
            rect = cv2.minAreaRect(largest_contour)
            box = np.int0(cv2.boxPoints(rect))
            print(rect, box)
        else:
            rect = []
            box = []

        if box != []:
            cv2.drawContours(image_from_camera, [box], 0, (0, 0, 255), 2)

            # Draw center dot
            rect1 = ((rect[0][0], rect[0][1]), (rect[1][0], rect[1][1]), rect[2])
            cv2.circle(image_from_camera, (int(rect1[0][0]), int(rect1[0][1])), 10, (0, 0, 255), -1)

            # Draw crosshair lines
            cv2.line(image_from_camera, (int(rect[0][0]), int(rect[0][1])), (int(rect[0][0]), FRAME_HEIGHT), (0, 0, 255), 2)
            cv2.line(image_from_camera, (int(rect[0][0]), int(rect[0][1])), (int(rect[0][0]), 0), (0, 0, 255), 2)
            cv2.line(image_from_camera, (int(rect[0][0]), int(rect[0][1])), (FRAME_WIDTH, int(rect[0][1])), (0, 0, 255), 2)
            cv2.line(image_from_camera, (int(rect[0][0]), int(rect[0][1])), (0, int(rect[0][1])), (0, 0, 255), 2)

        # cv2.imshow('image0', image_from_camera)
        # cv2.imshow('image1', image_grayscale)
        # cv2.imshow('image2', image_gaussian)
        cv2.imshow('image3', image_threshold)
        # cv2.imshow('image4', image_erode)
        # cv2.imshow('image5', image_dilation)

        cv2.imshow('image6', image_from_camera)

        key = cv2.waitKey(1)
        if key == ord('q'):
            break
        elif key == ord('g'):
            bypass_gaussian = False
        elif key == ord('t'):
            bypass_gaussian = True


    cv2.destroyAllWindows()
    camera0.release()
    print('Have a nice day! :)')


if __name__ == '__main__':
    main()
