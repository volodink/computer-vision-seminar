import cv2
import numpy as np


original_image = cv2.imread(
    'monroe.jpg'
)

print(f'Image shape: {original_image.shape}')

image_width, image_height, _ = original_image.shape

show_original_image = True

pixel_shift_value = 25

while True:
    key = cv2.waitKey(1)
    if key == ord('q'):
        break
    elif key == ord('1'):
        show_original_image = True
    elif key == ord('2'):
        show_original_image = False
    elif key == ord('='):
        if pixel_shift_value <= 100:
            pixel_shift_value += 1
    elif key == ord('-'):
        if pixel_shift_value >= 2:
            pixel_shift_value -= 1


    blank_channel = np.zeros(original_image.shape[:2], dtype='uint8')
    blank_image = cv2.merge((blank_channel, blank_channel, blank_channel))

    b, g, r = cv2.split(original_image)
    
    red = cv2.merge((r, blank_channel, blank_channel))
    green = cv2.merge((blank_channel, g, blank_channel))
    blue = cv2.merge((blank_channel, blank_channel, b))
    
    green_center = green.copy()
    
    blue_left = blank_image.copy()
    blue_left[:, 0:original_image.shape[1]-pixel_shift_value, :] = blue[:, pixel_shift_value:, :]

    red_right = blank_image.copy()
    red_right[:, pixel_shift_value:, :] = red[:, 0:-pixel_shift_value, :]

    processed_image = cv2.merge((cv2.split(blue_left)[2], cv2.split(green_center)[1], cv2.split(red_right)[0]))
    
    cropped_image = processed_image[:, pixel_shift_value:-pixel_shift_value, :]

    if show_original_image:
        cv2.imshow('img', original_image)
    else:
        cv2.imshow('img', processed_image)

cv2.destroyAllWindows()
