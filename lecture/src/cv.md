---
marp: true
theme: lection
paginate: true

---

<!-- _class: lead -->
<!-- _paginate: false -->

#

#

#

# Компьютерное и машинное зрение для начинающих: цели, задачи, примеры


#

#

#

#

#

#

#

##### Константин Володин

---
<!-- _paginate: false -->

# Немного о себе

## Володин Константин Игоревич

:snake: Python: люблю, применяю, преподаю

:robot::rocket: Разработчик встраиваемых систем

:sloth: Преподаватель САПР Altium Designer

:scroll: Выпускник ПензГТУ (ПГТА) с отличием

:eyes: Moscow State University CanSAT expert 

:exploding_head: MTA SQL, MCP, Intel NNGU PCF и т.п.

:ru: Немного преподаю на кафедре ИТС ПензГТУ

---
<!-- _paginate: false -->

# Кафедра и направления подготовки

1. Кафедра: __Информационные технологии и системы__

2. 09.03.02 Информационные системы и технологии

3. Образовательные программы:
_-_ Информационные системы в технике и технологиях 
_-_ Информационные технологии в дизайне
_-_ Безопасность информационных систем

3. Другие: [penzgtu.ru](penzgtu.ru)

![bg](img/misc/university.svg)

---

# План семинара

**Час 1** 
_-_ Разминка и общение :raising_hand:
_-_ Цели, задачи, определения и неожиданные открытия
_-_ Вспоминаем ~~внезапно учим заного~~ __Python__ :snake:

**Час 2** 
_-_ Изображения: пиксели, каналы, цвета, цветовые модели
_-_ Пишем генератор наложения эффекта на изображение в стиле Энди Ворхола 

**Час 3** Считаем количество желтых стикеров на изображении :memo::memo::memo:

**Час 4** Пишем простейший детектор __пожара__ в кадре :fire::fire::fire:

---
<!-- _paginate: false -->

![bg](img/temp/zagruzheno.png)

---
<!-- _paginate: false -->
<!-- _class: diagram -->

![bg w:1100px](img/misc/python-logo.png)

---

# Компьютерное vs Машинное зрение

![h:550px](img/cv/-1.gif)

---

# Компьютерное vs Машинное зрение

![h:550px](img/cv/2.gif)

---

# Компьютерное vs Машинное зрение

![h:550px](img/cv/0.gif)

---

# Компьютерное vs Машинное зрение

![h:550px](img/cv/9.gif)

---

# Компьютерное vs Машинное зрение

![h:600px](img/cv/3.gif)

---

# Компьютерное vs Машинное зрение

![h:550px](img/cv/robot-machine-learning.gif)

---

# Машинное зрение

![w:1200px](img/cv/Computer-vision-system-architecture-proposed-in-this-paper.png)

Источник: https://www.researchgate.net/figure/Computer-vision-system-architecture-proposed-in-this-paper_fig1_334393823

---

# Выделение информативных признаков и принятие решений

![w:1050px](img/cv/0_c9G3a5G1ambIebHk.png)

---
<!-- _paginate: false -->
<!-- _class: invert oneline -->

# <!-- fit -->cессия live-coding

---
<!-- _paginate: false -->

# Час 2 

# Пишем генератор наложения эффекта на изображение в стиле Энди Ворхола

---
<!-- _paginate: false -->

![bg](img/hour2/monroe.jpg)

![bg](img/hour2/monroe-shifted.png)

---

![bg w:700px](img/hour2/three_d_array.png)

--- 

![bg w:1100px](img/hour2/reign_pic_breakdown.png)

---

![bg w:1100px](img/hour2/monroe-shifted.png)

---
<!-- _paginate: false -->
<!-- _class: invert oneline -->

# <!-- fit -->cессия live-coding

---
<!-- _paginate: false -->

# Час 3 

# Считаем количество желтых стикеров :memo::memo::memo:

---

# Задача: посчитать количество желтых стикеров и вывести количество на экран

![w:590px](img/hour3/stickers.jpg) ![w:590px](img/hour3/stickers-boxed.png)

---
<!-- _paginate: false -->
<!-- _class: invert oneline -->

# <!-- fit -->cессия live-coding

---
<!-- _paginate: false -->

# Час 3 

# Пишем простейший детектор пожара в кадре :fire::fire::fire:

---
<!-- _class: diagram -->

# Виды беспилотных летательных аппаратов (БЛА)

![h:220px](img/fire/uav-plane.jpg) ![h:220px](img/fire/uav-multi.jpg)

![h:220px](img/fire/uav-vtol.jpg) ![h:220px](img/fire/uav-aerostat.jpg)
Еще немного:
https://youtu.be/0aHXrUHTxP0?t=112

---

# Промышленное применение

1. Инспекция: очаги пожара, ЛЭП, солнечные панели
2. Поиск людей
3. Пожаротушение

![h:270px](img/fire/fire-uav.png) ![h:270px](img/fire/lost.webp) ![h:270px](img/fire/fire-fight-2.jpg)

Источник: https://www.geekwire.com/2015/drone-helps-fight-washington-state-wildfires-in-latest-demonstration-of-the-technologys-potential/

---

# Тепловизоры

![h:455px](img/fire/flir-1.png)
Источник: What can I see with my thermal drone? | FLIR DELTA https://www.youtube.com/watch?v=CVE1HJ6n9g8

---

# Тепловизоры

![h:550px](img/fire/flir-2.png)

---
<!-- _paginate: false -->

![bg w:100%](img/fire/fire-coords.png)

---
<!-- _paginate: false -->

![bg w:100%](img/fire/best-screenshot.png)

---
<!-- _paginate: true -->

# Полосно-пропускающие оптические фильтры

Midopt bandpass filters

![h:340px](https://midopt.com/wp-content/uploads/2015/06/Bandpass_Logo_RGB-web.jpg) ![h:340px](https://midopt.com/wp-content/filter_image/MidOpt_BN850_Trans_Graphic.png) ![h:360px](img/fire/spectral-midopt.png)

Источник: https://midopt.com/filters/bandpass/
BN850 filter: https://midopt.com/filters/bn850/

---
<!-- _class: diagram -->
<!-- _paginate: true -->

# Основная идея

![](img/fire/spectrum.jpg)

Источник: https://www.monochromeimaging.com/technical/full-spectrum-ir/

---
<!-- _class: diagram -->
<!-- _paginate: true -->

# Конвейер обработки изображения с камеры

![w:1200px](diagrams/image-processing-pipeline.png)

![w:1200px](diagrams/image-processing-pipeline-2.png)

![w:1200px](diagrams/image-processing-pipeline-3.png)

---
<!-- _class: invert -->

![bg w:730px](img/fire/basic-image-proc.gif)


---
<!-- _class: invert -->

![bg w:70%](img/fire/new-detector.gif)

---
<!-- _paginate: false -->
<!-- _class: invert oneline -->

# <!-- fit -->cессия live-coding

---
<!-- _class: split -->

# Ссылки и материалы

# &nbsp;

# &nbsp;

<div class=ldiv>

## Базовый репозиторий мероприятия: 

https://gitlab.com/volodink/computer-vision-seminar

</div>
<div class=rdiv>

## &nbsp;&nbsp;&nbsp; QR-код

![w:500px](img/misc/qr-code.png)

</div>

---
<!-- _class: contacts -->

# Контакты

![w:96px](img/misc/gmail-logo.svg) &nbsp; volodin.konstantin@gmail.com

![w:96px](img/misc/vk-logo.svg.png)  &nbsp; vk.com/volodin.konstantin

![w:96px](img/misc/telegram-logo.svg) &nbsp; @volodink

---

<!-- _class: oneline -->
<!-- _class: lead -->
<!-- _paginate: false -->

# Благодарю за внимание!

## Вопросы?
